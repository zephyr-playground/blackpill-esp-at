# blackpill + esp-at (2.0) wifi shell sample

## Overview

This is a zephyr example application for the stm32f411ce a.k.a BlackPill board
connected to an esp8266 with the ESP-AT v2.0 firmware installed.
The Wi-Fi shell module provides a set of commands:
scan, connect, disconnect and ap.
It also enables the net_shell module to verify net_if settings.

## Hardware setup
The ME6206A33XG LDO from the blackpill will most probably not be able to provide enough
power to the esp8266 so it will need it's own power supply.
The blackpill and esp8266 are connected via uart using PA2 (TX) and PA3 (RX).


## Building and Running

```
git clone https://gitlab.com/zephyr-playground/blackpill-esp-at.git
west build -b blackpill_f411ce blackpill-esp-at
# put board into dfu-upload mode (press and hold BOOT0 while resetting it)
west flash
picocom -b 115200 /dev/ttyACM0
```

## Sample console interaction

```
shell> wifi scan
Scan requested
shell>
Num  | SSID                             (len) | Chan | RSSI | Sec
1    | kapoueh!                         8     | 1    | -93  | WPA/WPA2
2    | mooooooh                         8     | 6    | -89  | WPA/WPA2
3    | Ap-foo blob..                    13    | 11   | -73  | WPA/WPA2
4    | gksu                             4     | 1    | -26  | WPA/WPA2
----------
Scan request done

shell> wifi connect "gksu" 4 SecretStuff
Connection requested
shell>
Connected
shell>
```
